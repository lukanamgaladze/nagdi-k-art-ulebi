package com.example.nagdiqartulebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MenuActivity : AppCompatActivity() {

    lateinit var profileButton : Button
    lateinit var games : Button
    lateinit var map : Button
    lateinit var calculator : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        profileButton = findViewById(R.id.profileButton)
        games = findViewById(R.id.games)
        map = findViewById(R.id.googleMap)
        calculator = findViewById(R.id.calculator)

        profileButton.setOnClickListener {

            startActivity(Intent(this, ProfileActivity::class.java))

        }

        games.setOnClickListener {

            startActivity(Intent(this, MiniGamesActivity::class.java))

        }

        map.setOnClickListener {

            startActivity(Intent(this, MapActivity::class.java))

        }

        calculator.setOnClickListener {

            startActivity(Intent(this, CalculatorActivity::class.java))

        }

    }
}