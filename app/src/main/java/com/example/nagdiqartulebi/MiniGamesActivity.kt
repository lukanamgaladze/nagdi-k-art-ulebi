package com.example.nagdiqartulebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MiniGamesActivity : AppCompatActivity() {

    lateinit var game1Button : Button
    lateinit var game2Button : Button
    lateinit var game3Button : Button
    lateinit var back1 : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mini_games)

        game1Button = findViewById(R.id.game1Button)
        game2Button = findViewById(R.id.game2Button)
        game3Button = findViewById(R.id.game3Button)
        back1 = findViewById(R.id.back1)

        game1Button.setOnClickListener {

            startActivity(Intent(this, Game1Activity::class.java))

        }

        game2Button.setOnClickListener {

            startActivity(Intent(this, Game2Activity::class.java))

        }

        game3Button.setOnClickListener {

            startActivity(Intent(this, Game3Activity::class.java))

        }

        back1.setOnClickListener {

            startActivity(Intent(this, MenuActivity::class.java))

        }

    }
}