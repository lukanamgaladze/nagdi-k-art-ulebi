package com.example.nagdiqartulebi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ResetPasswordActivity : AppCompatActivity() {

    private lateinit var Reset1 : EditText
    private lateinit var Reset2 : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        init()
        listeners()

    }

    private fun init(){

        Reset1 = findViewById(R.id.Reset1)
        Reset2 = findViewById(R.id.Reset2)

    }

    private fun listeners(){

        Reset2.setOnClickListener {

            val email = Reset1.text.toString()

            if (email.isEmpty()){

                Toast.makeText(this, "ჩაწერე ემაილი!!!", Toast.LENGTH_LONG).show()
                return@setOnClickListener

            }

            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->

                    if(task.isSuccessful){

                        Toast.makeText(this, "შეამოწმე მალის ფოსტა", Toast.LENGTH_LONG).show()

                    }else{

                        Toast.makeText(this, "ახლიდან სცადე", Toast.LENGTH_LONG).show()

                    }


                }

        }

    }

}