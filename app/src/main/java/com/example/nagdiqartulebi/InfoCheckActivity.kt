package com.example.nagdiqartulebi

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class InfoCheckActivity : AppCompatActivity() {

    lateinit var BackToGames : Button
    lateinit var NameUser : TextView
    lateinit var Age : TextView
    lateinit var edu : TextView
    lateinit var profession : TextView

    private lateinit var db : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_check)

        BackToGames = findViewById(R.id.BackToGames)
        NameUser = findViewById(R.id.NameUser)
        Age = findViewById(R.id.Age)
        edu = findViewById(R.id.edu)
        profession = findViewById(R.id.profession)

        val saxeli = NameUser.text.toString()
        val asaki = Age.text.toString()
        val ganatleba = edu.text.toString()
        val profesia = profession.text.toString()

        realData(saxeli)

        BackToGames.setOnClickListener {

            startActivity(Intent(this, MiniGamesActivity::class.java))

        }

    }

    private fun realData(saxeli: String){

        db = FirebaseDatabase.getInstance().getReference("UserInfo")
        db.child(saxeli).get().addOnSuccessListener {

            if (it.exists()){

                val saxeli = it.child("saxeli").value
                val asaki = it.child("asaki").value
                val ganatleba = it.child("ganatleba").value
                val profesia = it.child("profesia").value

            }else{

                Toast.makeText(this, "ასეთი მომხმარებელი ვერ მოიძებნა", Toast.LENGTH_LONG).show()

            }

        }.addOnFailureListener {

            Toast.makeText(this, "პრგრამამ კრახი განიცადა", Toast.LENGTH_LONG).show()

        }

    }

}