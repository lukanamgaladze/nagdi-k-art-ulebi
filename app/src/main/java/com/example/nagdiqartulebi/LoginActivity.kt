package com.example.nagdiqartulebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    lateinit var loginEmail : EditText
    lateinit var loginPassword : EditText
    lateinit var loginAuth : Button
    lateinit var loginReset : TextView
    lateinit var loginReg : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
        listeners()

    }

    private fun init(){

        loginEmail = findViewById(R.id.loginEmail)
        loginPassword = findViewById(R.id.loginPassword)
        loginAuth = findViewById(R.id.loginAuth)
        loginReset = findViewById(R.id.loginReset)
        loginReg = findViewById(R.id.loginReg)
    }

    private fun listeners(){

        loginReg.setOnClickListener {

            startActivity(Intent(this,RegistrationActivity::class.java))

        }

        loginReset.setOnClickListener {

            startActivity(Intent(this, ResetPasswordActivity::class.java))

        }

        loginAuth.setOnClickListener {

            val email = loginEmail.text.toString()
            val password = loginPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()){

                Toast.makeText(this, "რაღაც რიგზე ვერაა", Toast.LENGTH_LONG).show()
                return@setOnClickListener

            }

            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->

                    if(task.isSuccessful){

                        goToMenu()

                    }else{

                        Toast.makeText(this, "რაღაც რიგზე ვერაა", Toast.LENGTH_LONG).show()

                    }

                }

        }

    }

    private fun goToMenu() {

        startActivity(Intent(this, MenuActivity::class.java))
        finish()

    }

}