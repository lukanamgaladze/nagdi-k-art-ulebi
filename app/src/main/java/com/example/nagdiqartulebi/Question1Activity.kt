package com.example.nagdiqartulebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Question1Activity : AppCompatActivity() {

    lateinit var answer1 : EditText
    lateinit var answer2 : EditText
    lateinit var answer3 : EditText
    lateinit var answer4 : EditText
    lateinit var QuestionButton1 : Button

    lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question1)

        answer1 = findViewById(R.id.answer1)
        answer2 = findViewById(R.id.answer2)
        answer3 = findViewById(R.id.answer3)
        answer4 = findViewById(R.id.answer4)
        QuestionButton1 = findViewById(R.id.QuestionButton1)

        QuestionButton1.setOnClickListener {

            val saxeli = answer1.text.toString()
            val asaki = answer2.text.toString()
            val ganatleba = answer3.text.toString()
            val profesia = answer4.text.toString()

            database = FirebaseDatabase.getInstance().getReference("UserInfo")
            val UserInfo = UserInfo(saxeli, asaki, ganatleba, profesia)
            database.child(saxeli).setValue(UserInfo)

            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)

        }

    }

}