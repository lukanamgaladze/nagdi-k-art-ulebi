package com.example.nagdiqartulebi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ChangePasswordActivity : AppCompatActivity() {
    lateinit var changePassword : EditText
    lateinit var changeButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        init()

    }

    private fun init(){

        changePassword = findViewById(R.id.changePassword)
        changeButton = findViewById(R.id.changeButton)

        changeButton.setOnClickListener {

            val changedPassword = changePassword.text.toString()

            if (changedPassword.isEmpty() || changedPassword.length < 8){

                Toast.makeText(this, "რაღაც რიგზე ვერაა", Toast.LENGTH_LONG).show()
                return@setOnClickListener

            }

            FirebaseAuth.getInstance().currentUser?.updatePassword(changedPassword)
                ?.addOnCompleteListener { task ->

                    if(task.isSuccessful){

                        Toast.makeText(this, "წარმატებულია", Toast.LENGTH_LONG).show()

                    }else {

                        Toast.makeText(this, "რაღაც რიგზე ვერაა", Toast.LENGTH_LONG).show()

                    }

                }

        }

    }

}