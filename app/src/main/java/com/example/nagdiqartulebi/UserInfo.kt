package com.example.nagdiqartulebi

data class UserInfo(

    val saxeli : String? = null,
    val asaki : String? = null,
    val gantleba : String? = null,
    val profesia : String? = null

)
