package com.example.nagdiqartulebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileActivity : AppCompatActivity() {

    private lateinit var ProfileChangePicture : Button
    private lateinit var ProfileChangePassword : Button
    private lateinit var AccLogOut : Button
    private lateinit var ImageView : ImageView
    private lateinit var avatarURL : EditText
    private lateinit var Username : TextView
    private lateinit var UsernameEdit : EditText

    // private lateinit var AccInfoBut : Button

    private val auth = FirebaseAuth.getInstance()
    private var db = FirebaseDatabase.getInstance().getReference("userInfo")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        init()
        listeners()

        db.child(auth.currentUser?.uid!!).addValueEventListener(object: ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                val userInfo = snapshot.getValue(User::class.java) ?: return
                Username.text = userInfo.username
                Glide.with(this@ProfileActivity).load(userInfo.imageUrl).into(ImageView)
            }

            override fun onCancelled(error: DatabaseError) {

            }


        })

    }

    private fun init(){

        ProfileChangePicture = findViewById(R.id.ProfileChangePicture)
        ProfileChangePassword = findViewById(R.id.ProfileChangePassword)
        AccLogOut = findViewById(R.id.AccLogOut)
        ImageView = findViewById(R.id.imageView)
        avatarURL = findViewById(R.id.avatarURL)
        Username = findViewById(R.id.Username)
        UsernameEdit = findViewById(R.id.UsernameEdit)

        // AccInfoBut = findViewById(R.id.AccInfoBut)

    }

    private fun listeners(){

        ProfileChangePassword.setOnClickListener {

            startActivity(Intent(this, ChangePasswordActivity::class.java))

        }

        ProfileChangePicture.setOnClickListener {

            val url = avatarURL.text.toString()
            val username = UsernameEdit.text.toString()

            val userInfo = User(url, username)
            db.child(auth.currentUser?.uid!!).setValue(userInfo)

        }

      AccLogOut.setOnClickListener {

            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()

      }

    }

}