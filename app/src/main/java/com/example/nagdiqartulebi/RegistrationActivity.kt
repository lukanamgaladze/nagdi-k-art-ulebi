package com.example.nagdiqartulebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {

    lateinit var regEmail : EditText
    lateinit var regPassword : EditText
    lateinit var regButton : Button

    lateinit var alreadyReg : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        init()
        listeners()

    }

    private fun init(){

        regEmail = findViewById(R.id.regEmail)
        regPassword = findViewById(R.id.regPassword)
        regButton = findViewById(R.id.regButton)
        alreadyReg = findViewById(R.id.alreadyReg)

    }

    private fun listeners(){

        regButton.setOnClickListener {

            val email = regEmail.text.toString()
            val password = regPassword.text.toString()

            if (email.isEmpty() || password.isEmpty() || password.length < 7){

                Toast.makeText(this, "ემაილი ან პაროლი არასწორია", Toast.LENGTH_LONG).show()
                return@setOnClickListener

            }

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->

                    if(task.isSuccessful){

                        Toast.makeText(this, "რეგისტრაცია წარმატებულია", Toast.LENGTH_LONG).show()
                        startActivity(Intent(this, Question1Activity::class.java))
                        finish()

                    }else{

                        Toast.makeText(this, "რაღაც რიგზე ვერაა", Toast.LENGTH_LONG).show()

                    }

                }

        }

        alreadyReg.setOnClickListener {

            startActivity(Intent(this, LoginActivity::class.java))
            finish()

        }

    }

}